<?php 
	session_start();
	/**
	 * summary
	 */
	class Controller 
	{
	    /**
	     * summary
	     */
	    public $model;
	    public function __construct()
	    {
	        require "model/Model.php";
			//$m  = new Model();
			$this->model = new Model();
	    }

	    public function index()
	    {
	    	
	    	$dataHome =  $this->model->getData();
			require "views/home.php";	
	    }

	    public function login()
	    {
	    	require "views/login.php";
	    }

	    function logout()
	    {
	    	unset($_SESSION['user']);
	    	header("refresh:0;?page=login");
	    }

	    function doLogin()
	    {

	    	$res =  $this->model->login(
	    		$_POST['user'],
	    		 md5( $_POST['pass'])
	    		// $_POST['pass']
	    		);

	    	if($res ){
	    		$_SESSION["user"] = $res;
	    		header("refresh:5;?page=home");
	    	}else {
	    		echo 'khong thanh cong';
	    		// header("refresh:3;?page=home");
	    	}
	    }

	    public function add()
	    {
	    	require "views/add.php";
	    }

	    public function doAdd()
	    {
	    	if($this->model->insert($_POST['msv'], $_POST['ten'], $_POST['tuoi'], $_POST['quequan'], $_POST['gt']) ){
	    		echo 'Them thanh cong';
	    		header("refresh:1;?page=home");
	    	}else {
	    		echo 'khong thanh cong';
	    	}
	    }

	    public function delete()
	    {
	    	if($this->model->delete( $_GET['id']) ){
	    		echo 'Thanh cong';
	    		header("refresh:0.2;?page=home");
	    	}else {
	    		// echo 'khong thanh cong';
	    	}
	    }

	    public function search()
	    {
	    	$dataHome =  $this->model->search($_GET['s']);
	    	// $this->log($dataHome->fetch_assoc());
			require "views/home.php";	
	    }

	    function search2()
	    {
	    	$dataHome =  $this->model->search($_GET['s']);
	    	// $this->log($dataHome->fetch_assoc());
			require "views/home.php";
	    }

	    public function edit()
	    {
	    	$msv = $_GET['id'];

	    	$dataStudentByID = $this->model->getSutudenByMSV($_GET['id']);

	    	require "views/add.php";
	    }

	    public function doEdit()
	    {
	    	// edit($msv, $ten, $tuoi, $quequan, $gt)
	    	if($this->model->edit($_POST['msv'], $_POST['ten'], $_POST['tuoi'], $_POST['quequan'], $_POST['gt']) ){
	    		echo 'sua thanh cong';
	    		header("refresh:1;?page=home");
	    	}else {
	    		echo 'khong thanh cong';
	    	}
	    }

	    private function log($val)
		{
			echo '<pre>';
			var_dump($val);
			echo '</pre>';
		}
	}
 ?>